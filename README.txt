CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Field collection load module displays the entire array of the fields in a field collection of a node. This module loads the fields added in the field collection and appends the array under the field in the node array (By default drupall shows the target id of the fields in under a field collection). So when a node load is done the user gets the field collection array appended to the node. Similarly this functionality works same for the user_load and taxonomy_term_load.

To submit bug reports and feature suggestions, or to track changes:
http://drupal.org/project/issues/field_collection_load


REQUIREMENTS
------------

No special requirements


INSTALLATION
------------

Install as you would normally install a contributed Drupal. See:
https://drupal.org/documentation/install/modules-themes/modules-7 for further
information.


CONFIGURATION
-------------

Navigate to Configuration >> Content authoring >> list bundles, you will see a configuration form displaying the list of nodes, vacabularies and the users.Select the type of nodes, taxonomy vocabularies and users on which you want to load the field collection and savee the configuration settings


MAINTAINERS
-----------

Current maintainers:
 * Karan Sen - https://www.drupal.org/user/3392998



This project has been sponsored by:
 * Srijan Technologies
   Srijan is the largest pure-play Drupal company in Asia-Pacific. Srijan specializes in building high-traffic websites and complex web applications in Drupal and has been serving clients across USA, Asia, Europe, Australia and Japan.
