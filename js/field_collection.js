(function ($) {
  Drupal.behaviors.FieldCollectionModule = {
    attach: function (context, settings) {
      $('#edit-node-bundles-all').change(function(event) {   
        if(this.checked) {
            // Iterate each checkbox
            $('#edit-node-bundles .form-type-checkbox input:checkbox').each(function() {
                this.checked = true;                        
            });
            $('#edit-bundles .form-type-checkbox input:checkbox').each(function() {
                this.checked = false;                        
            });
        }
    if(!this.checked) {
            // Iterate each checkbox
            $('#edit-node-bundles .form-type-checkbox input:checkbox').each(function() {
                this.checked = false;                        
            });
        }
});

    $('.form-item-bundles .form-checkbox').click(function() {
        var check_value = $(this).val();
        
        var input_selector = 'input[value=' + check_value + ']';
        var div_node = '#edit-node-bundles ' +input_selector;
        $(div_node).removeAttr('checked',false);
        
    });

    $('.form-item-node-bundles .form-checkbox').click(function() {

        var check_value = $(this).val();
        
        var input_selector = 'input[value=' + check_value + ']';
        var div_node = '#edit-bundles ' +input_selector;
        $(div_node).removeAttr('checked',false);
        
    });
    }
  };
}(jQuery));



